class Countries extends HTMLElement{
  constructor(){
      super();
  }

  async fetchApi() {
    const response = await fetch("https://restcountries.com/v3.1/all")
    const data = await response.json()
    return data
  }

  connectedCallback(){
    this.fetchApi().then(data => {
      console.log(data)
      data.slice(0, 12).forEach(current => {
        let { flags, name, capital, population, continents, languages } = current;
        this.innerHTML += `<country-card
          class="country-card"
          png="${flags.png}" 
          name="${name.common}" 
          capital="${capital}"
          population="${population}"
          continent="${continents}"
        </country-card>`;
     });
    }) 
  } 
}

class CountriesChild extends HTMLElement{
  constructor(){
      super();
  }

  connectedCallback(){
    this.innerHTML = `<a href="#openModal${this.getAttribute("name")}" class="card-link">
        <div class="card">
          <div class="card__image">
            <img class="image" src="${this.getAttribute("png")}"/>
          </div>
          <h3 class="card__title">${this.getAttribute("name")}</h3>
          <span class="card__text">Capital: ${this.getAttribute("capital")}</span>
          <span class="card__text">Population: ${this.getAttribute("population")}</span>
        </div>
      </a> 

      <div id="openModal${this.getAttribute("name")}" class="modal-dialog">
        <div class="modal">	
          <a href="#close" title="Close" class="modal--close">X</a>
          <h3 class="modal__title">${this.getAttribute("name")}</h3>
          <p class="modal__description">Is located on the continent: ${this.getAttribute("continent")}</p>
        </div>
      </div>`;
  }
}

customElements.define("country-card", CountriesChild);
customElements.define("countries-custom", Countries);